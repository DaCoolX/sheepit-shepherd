#!/bin/bash
set -e

if [ "$1" = 'apache2-foreground' ]; then
    service rsyslog start
    service cron start
fi

exec docker-php-entrypoint "$@"
