<?php

namespace App\Tests\Repository;

use App\Entity\Blend;
use App\Entity\Task;
use App\Repository\BlendRepository;
use App\Repository\TaskRepository;
use App\Tests\ToolsService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskRepositoryTest extends KernelTestCase {
    private EntityManager $entityManager;
    private BlendRepository $blendRepository;
    private TaskRepository $taskRepository;
    private ToolsService $toolsService;

    protected function setUp(): void {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->blendRepository = self::$container->get('App\Repository\BlendRepository');
        $this->taskRepository = self::$container->get('App\Repository\TaskRepository');
        $this->toolsService = new ToolsService(self::$container->get('App\Service\BlendService'));
    }

    public function testFindEmpty(): void {
        $this->assertEquals(0, $this->taskRepository->count([]));

        $this->assertNull($this->taskRepository->findOneAvailable());
    }

    public function testPriorityOneWithRunningTask(): void {
        // order:
        // 1. mp4/zip if all generate_frame are done for a project
        // 2. generate_frame
        // 3. thumbnail&delete

        $this->createBlendAndTasks();
        $this->assertTrue($this->taskRepository->count([]) > 0);

        // another runner is 'working' on the generate_frame
        foreach ($this->taskRepository->findAll() as $task) {
            /** @var Task $task */
            if ($task->getType() == Task::TYPE_GENERATE_FRAME) {
                $task->setStatus(Task::STATUS_RUNNING);
                //   $this->entityManager->remove($task);
            }

            $this->entityManager->flush();
        }

        // all generate_frame are down, we must NOT get a mp4/zip task because all generate_frame are not done
        $task = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task);

        $this->assertFalse(in_array($task->getType(), [Task::TYPE_GENERATE_MP4_FINAL, Task::TYPE_GENERATE_MP4_PREVIEW, Task::TYPE_GENERATE_ZIP, Task::TYPE_DELETE_BLEND]));
    }

    public function testPriorityOneWithNoRunningTask(): void {
        // order:
        // 1. mp4/zip if all generate_frame are done for a project
        // 2. generate_frame
        // 3. thumbnail&delete

        $this->createBlendAndTasks();
        $this->assertTrue($this->taskRepository->count([]) > 0);

        // 'work' on the generate_frame and remove them
        foreach ($this->taskRepository->findAll() as $task) {
            /** @var Task $task */
            if ($task->getType() == Task::TYPE_GENERATE_FRAME) {
                $this->entityManager->remove($task);
            }

            $this->entityManager->flush();
        }

        // all generate_frame are down, we must get a mp4/zip task
        $task = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task);

        $this->assertTrue(in_array($task->getType(), [Task::TYPE_GENERATE_MP4_FINAL, Task::TYPE_GENERATE_MP4_PREVIEW, Task::TYPE_GENERATE_ZIP]));
    }

    public function testPriorityTwo(): void {
        // order:
        // 1. mp4/zip if all generate_frame are done for a project
        // 2. generate_frame
        // 3. thumbnail&delete

        $this->createBlendAndTasks();
        $this->assertTrue($this->taskRepository->count([]) > 0);

        $task = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task);

        $this->assertEquals(Task::TYPE_GENERATE_FRAME, $task->getType());
    }

    public function testPriorityThree(): void {
        // order:
        // 1. mp4/zip if all generate_frame are done for a project
        // 2. generate_frame
        // 3. thumbnail&delete

        $this->createBlendAndTasks();
        $this->assertTrue($this->taskRepository->count([]) > 0);

        // 'work' on the generate_frame and remove them
        foreach ($this->taskRepository->findAll() as $task) {
            /** @var Task $task */
            if ($task->getType() == Task::TYPE_GENERATE_FRAME) {
                $task->setStatus(Task::STATUS_RUNNING);
                //    $this->entityManager->remove($task);
            }

            $this->entityManager->flush();
        }

        // all generate_frame are down, we must get a mp4/zip task
        $task = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task);

        $this->assertTrue(in_array($task->getType(), [Task::TYPE_DELETE_BLEND, Task::TYPE_GENERATE_TILE_THUMBNAIL, Task::TYPE_GENERATE_FRAME_THUMBNAIL]));
    }

    public function testOnlyOneHeavyIOTaskAtTheTime(): void {
        // ZIP or MP4 are really heavy task

        $this->createBlendAndTasks();
        $this->assertTrue($this->taskRepository->count([]) > 0);

        // 'work' on the generate_frame and remove them
        foreach ($this->taskRepository->findAll() as $task) {
            /** @var Task $task */
            if ($task->getType() == Task::TYPE_GENERATE_FRAME) {
                $this->entityManager->remove($task);
            }

            $this->entityManager->flush();
        }

        // all generate_frame are down, we must get a mp4/zip task
        $task1 = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task1);

        $this->assertTrue(in_array($task1->getType(), [Task::TYPE_GENERATE_MP4_FINAL, Task::TYPE_GENERATE_MP4_PREVIEW, Task::TYPE_GENERATE_ZIP]));
        $task1->setStatus(Task::STATUS_RUNNING);
        $this->entityManager->flush();

        // 2nd task can NOT be an heavy task
        $task2 = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task2);

        $this->assertFalse(in_array($task2->getType(), [Task::TYPE_GENERATE_MP4_FINAL, Task::TYPE_GENERATE_MP4_PREVIEW, Task::TYPE_GENERATE_ZIP]));
    }

    public function testFoundDeadTasksWithDeadProcess(): void {
        $blend = $this->createBlendAndTasks();
        $this->assertIsObject($blend);

        $this->assertCount(0, $this->taskRepository->findDeadTasks());

        // add a dead task
        $task = new Task();
        $task->setBlend($blend);
        $task->setPID(123456789);
        $task->setType(Task::TYPE_GENERATE_ZIP);
        $task->setStatus(Task::STATUS_RUNNING);
        $task->setStartTime((new \DateTime())->setTimestamp(time() - Task::MAX_RUNNING_TIME - 100));

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        $this->assertCount(1, $this->taskRepository->findDeadTasks());

    }

    public function testFoundDeadTasksWithAliveProcess(): void {
        $blend = $this->createBlendAndTasks();
        $this->assertIsObject($blend);

        $this->assertCount(0, $this->taskRepository->findDeadTasks());

        // add a dead task
        $task = new Task();
        $task->setBlend($blend);
        $task->setPID(getmypid());
        $task->setType(Task::TYPE_GENERATE_ZIP);
        $task->setStatus(Task::STATUS_RUNNING);
        $task->setStartTime((new \DateTime())->setTimestamp(time() - Task::MAX_RUNNING_TIME - 100));

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        $this->assertCount(0, $this->taskRepository->findDeadTasks());

    }

    public function testTaskPositionQueueFor(): void {
        $blend = $this->createBlendAndTasks();
        $this->assertIsObject($blend);

        // createBlendAndTasks only added some generate_frame task but not zip or mp4, add them manually
        $taskMP4Final = new Task();
        $taskMP4Final->setType(Task::TYPE_GENERATE_MP4_FINAL);
        $taskMP4Final->setBlend($blend);

        $taskMP4Preview = new Task();
        $taskMP4Preview->setType(Task::TYPE_GENERATE_MP4_PREVIEW);
        $taskMP4Preview->setBlend($blend);

        $taskZip = new Task();
        $taskZip->setType(Task::TYPE_GENERATE_ZIP);
        $taskZip->setBlend($blend);

        $this->entityManager->persist($taskMP4Final);
        $this->entityManager->persist($taskMP4Preview);
        $this->entityManager->persist($taskZip);
        $this->entityManager->flush();

        [$total_task, $queue_position, $task_count] = $this->taskRepository->getTaskPositionQueueFor($blend);
        $this->assertEquals(10 + 10 * 4 + 3 + 3, $total_task);// generate_frame + thumbs + zip + mp4s
        $this->assertEquals(10 + 10 * 4 + 3, $queue_position);// generate_frame + thumbs  + zip + mp4s
        $this->assertEquals(10 + 10 * 4 + 3 + 3, $task_count); // generate_frame + thumbs  + zip + mp4s
    }

    private function createBlendAndTasks(): Blend {
        $blendId = $this->toolsService->addBlend("layer", 10, 4, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        // simulate render of frame (only add task)
        foreach ($blend->getFrames() as $frame) {
            $taskFrame = new Task();
            $taskFrame->setType(Task::TYPE_GENERATE_FRAME);
            $taskFrame->setFrame($frame);
            $taskFrame->setBlend($frame->getBlend());
            $this->entityManager->persist($taskFrame);

            foreach ($frame->getTiles() as $tile) {
                $taskTile = new Task();
                $taskTile->setType(Task::TYPE_GENERATE_FRAME_THUMBNAIL);
                $taskTile->setFrame($tile->getFrame());
                $taskTile->setBlend($tile->getFrame()->getBlend());
                $this->entityManager->persist($taskTile);
            }

            $this->entityManager->flush();

        }

        $taskMP4Final = new Task();
        $taskMP4Final->setType(Task::TYPE_GENERATE_MP4_FINAL);
        $taskMP4Final->setBlend($blend);

        $taskMP4Preview = new Task();
        $taskMP4Preview->setType(Task::TYPE_GENERATE_MP4_PREVIEW);
        $taskMP4Preview->setBlend($blend);

        $this->entityManager->persist($taskMP4Final);
        $this->entityManager->persist($taskMP4Preview);

        $taskZip = new Task();
        $taskZip->setType(Task::TYPE_GENERATE_ZIP);
        $taskZip->setBlend($blend);

        $this->entityManager->persist($taskZip);
        $this->entityManager->flush();

        return $blend;
    }
}