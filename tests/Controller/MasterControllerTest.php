<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MasterControllerTest extends WebTestCase {

    public function testCreateBlendFullWithoutToken(): void {
        $client = static::createClient();
        $client->request(
            'POST',
            '/master/blend',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{ "blend": 63, "framerate": "24.0", "mp4": "1", "width": 1920, "height": 1080, "image_extension": "png", "frames": [ { "type": "full", "uid": "792", "number": "0001", "image_extension": "png", "token": "5e80b5bc462825.60064857" }, { "type": "full", "uid": "793", "number": "0002", "image_extension": "png", "token": "5e80b5bc462a05.39790910" }, { "type": "full", "uid": "794", "number": "0003", "image_extension": "png", "token": "5e80b5bc462b66.22116777" }, { "type": "full", "uid": "795", "number": "0004", "image_extension": "png", "token": "5e80b5bc462cc0.20562031" }, { "type": "full", "uid": "796", "number": "0005", "image_extension": "png", "token": "5e80b5bc462e16.24456896" }, { "type": "full", "uid": "797", "number": "0006", "image_extension": "png", "token": "5e80b5bc462f77.88489856" }, { "type": "full", "uid": "798", "number": "0007", "image_extension": "png", "token": "5e80b5bc4630c9.41744127" }, { "type": "full", "uid": "799", "number": "0008", "image_extension": "png", "token": "5e80b5bc463214.49318587" }, { "type": "full", "uid": "800", "number": "0009", "image_extension": "png", "token": "5e80b5bc463361.79562545" }, { "type": "full", "uid": "801", "number": "0010", "image_extension": "png", "token": "5e80b5bc4634b6.58967668" } ] }'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateBlendFullWithToken(): void {
        $client = static::createClient();
        $client->request(
            'POST',
            '/master/blend',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{ "blend": 63, "token_owner": "1234", "token_owner_validity": 1789706972, "token_thumbnail_validity": 1884401372, "token_thumbnail": "5678", "framerate": "24.0", "mp4": "1", "width": 1920, "height": 1080, "image_extension": "png", "frames": [ { "type": "full", "uid": "792", "number": "0001", "image_extension": "png", "token": "5e80b5bc462825.60064857" }, { "type": "full", "uid": "793", "number": "0002", "image_extension": "png", "token": "5e80b5bc462a05.39790910" }, { "type": "full", "uid": "794", "number": "0003", "image_extension": "png", "token": "5e80b5bc462b66.22116777" }, { "type": "full", "uid": "795", "number": "0004", "image_extension": "png", "token": "5e80b5bc462cc0.20562031" }, { "type": "full", "uid": "796", "number": "0005", "image_extension": "png", "token": "5e80b5bc462e16.24456896" }, { "type": "full", "uid": "797", "number": "0006", "image_extension": "png", "token": "5e80b5bc462f77.88489856" }, { "type": "full", "uid": "798", "number": "0007", "image_extension": "png", "token": "5e80b5bc4630c9.41744127" }, { "type": "full", "uid": "799", "number": "0008", "image_extension": "png", "token": "5e80b5bc463214.49318587" }, { "type": "full", "uid": "800", "number": "0009", "image_extension": "png", "token": "5e80b5bc463361.79562545" }, { "type": "full", "uid": "801", "number": "0010", "image_extension": "png", "token": "5e80b5bc4634b6.58967668" } ] }'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateBlendLayer(): void {
        $client = static::createClient();
        $client->request(
            'POST',
            '/master/blend',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{ "blend": 87, "framerate": "24.0", "width": 1920, "height": 1080, "mp4": "1", "image_extension": "png", "frames": [ { "type": "layer", "uid": 0, "number": "0001", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "903", "number": "0000", "image_extension": "png", "token": "5e80b5bc462825.60064857" }, { "type": "layer", "uid": "904", "number": "0001", "image_extension": "png", "token": "5e80b5bc462825.60064856" }, { "type": "layer", "uid": "905", "number": "0002", "image_extension": "png", "token": "5e80b5bc462825.60064855" }, { "type": "layer", "uid": "906", "number": "0003", "image_extension": "png", "token": "5e80b5bc462825.60064854" } ] }, { "type": "layer", "uid": 0, "number": "0002", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "907", "number": "0000", "image_extension": "png", "token": "5e80b5bc462825.60064851" }, { "type": "layer", "uid": "908", "number": "0001", "image_extension": "png", "token": "5e80b5Ec462825.60064855" }, { "type": "layer", "uid": "909", "number": "0002", "image_extension": "png", "token": "5e8045bc462825.60064855" }, { "type": "layer", "uid": "910", "number": "0003", "image_extension": "png", "token": "540b5bc462825.60064855" } ] }, { "type": "layer", "uid": 0, "number": "0003", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "911", "number": "0000", "image_extension": "png", "token": "5e80b54bc462825.60064855" }, { "type": "layer", "uid": "912", "number": "0001", "image_extension": "png", "token": "5380b5bc462825.60064855" }, { "type": "layer", "uid": "913", "number": "0002", "image_extension": "png", "token": "5e804bc462825.60064855" }, { "type": "layer", "uid": "914", "number": "0003", "image_extension": "png", "token": "2e80b5bc462825.60064855" } ] }, { "type": "layer", "uid": 0, "number": "0004", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "915", "number": "0000", "image_extension": "png", "token": "5e80b6bc462825.60064855" }, { "type": "layer", "uid": "916", "number": "0001", "image_extension": "png", "token": "5e80b63c462825.60064855" }, { "type": "layer", "uid": "917", "number": "0002", "image_extension": "png", "token": "5e80b5bc44425.60064855" }, { "type": "layer", "uid": "918", "number": "0003", "image_extension": "png", "token": "5e80b5bc462825.60224855" } ] }, { "type": "layer", "uid": 0, "number": "0005", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "919", "number": "0000", "image_extension": "png", "token": "52280b5bc462825.60064855" }, { "type": "layer", "uid": "920", "number": "0001", "image_extension": "png", "token": "5e80b5bc462825.60033855" }, { "type": "layer", "uid": "921", "number": "0002", "image_extension": "png", "token": "5e80222c462825.60064855" }, { "type": "layer", "uid": "922", "number": "0003", "image_extension": "png", "token": "5e80b5bc46233445.60064855" } ] } ] }'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateBlendRegion(): void {
        $client = static::createClient();
        $client->request(
            'POST',
            '/master/blend',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],

            '{"blend":81,"framerate":"24.0","width":1920,"height":1080,"mp4":"1","image_extension":"png","frames":[{"type":"border","uid":0,"number":"0001","image_extension":"png","tiles":[{"uid":"883","number":"0000","image_extension":"png","token":"5e80b63c462825.6014855"},{"uid":"884","number":"0001","image_extension":"png","token":"5e80b63c462825.6024855"},{"uid":"885","number":"0002","image_extension":"png","token":"5e80b63c462825.60464855"},{"uid":"886","number":"0003","image_extension":"png","token":"5e80b63c462825.6464855"}]},{"type":"border","uid":0,"number":"0002","image_extension":"png","tiles":[{"uid":"887","number":"0000","image_extension":"png","token":"5e80b63c462825.61064855"},{"uid":"888","number":"0001","image_extension":"png","token":"5e80b63c462825.60264855"},{"uid":"889","number":"0002","image_extension":"png","token":"5e80b63262825.60064855"},{"uid":"890","number":"0003","image_extension":"png","token":"5e80263c462825.60064855"}]},{"type":"border","uid":0,"number":"0003","image_extension":"png","tiles":[{"uid":"891","number":"0000","image_extension":"png","token":"5e802c462825.60064855"},{"uid":"892","number":"0001","image_extension":"png","token":"5e84b63c462825.60064855"},{"uid":"893","number":"0002","image_extension":"png","token":"5e80b66462825.60064855"},{"uid":"894","number":"0003","image_extension":"png","token":"5e80b638462825.60064855"}]},{"type":"border","uid":0,"number":"0004","image_extension":"png","tiles":[{"uid":"895","number":"0000","image_extension":"png","token":"5e80b63c962825.60064855"},{"uid":"896","number":"0001","image_extension":"png","token":"5e8093c462825.60064855"},{"uid":"897","number":"0002","image_extension":"png","token":"5e80b6962825.60064855"},{"uid":"898","number":"0003","image_extension":"png","token":"5e80b63c499825.60064855"}]},{"type":"border","uid":0,"number":"0005","image_extension":"png","tiles":[{"uid":"899","number":"0000","image_extension":"png","token":"5e80b699462825.60064855"},{"uid":"900","number":"0001","image_extension":"png","token":"5e80b63c499925.60064855"},{"uid":"901","number":"0002","image_extension":"png","token":"5e8099b63c462825.60064855"},{"uid":"902","number":"0003","image_extension":"png","token":"5e89963c462825.60064855"}]}]}'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
