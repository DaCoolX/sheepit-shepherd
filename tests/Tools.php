<?php

namespace App\Tests;

use App\Entity\Tile;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use App\Service\FrameService;
use Doctrine\ORM\EntityManagerInterface;
use Imagick;
use ImagickPixel;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Tools {

    private EntityManagerInterface $entityManager;
    private TileRepository $tileRepository;
    private FrameRepository $frameRepository;
    private FrameService $frameService;

    public function __construct(EntityManagerInterface $entityManager, TileRepository $tileRepository, FrameRepository $frameRepository, FrameService $frameService) {
        $this->entityManager = $entityManager;
        $this->tileRepository = $tileRepository;
        $this->frameRepository = $frameRepository;
        $this->frameService = $frameService;
    }

    public function generateFakeImage(int $width, int $height): string {
        $path = sys_get_temp_dir().'/'.uniqid().'.png';

        $img = new Imagick();
        $img->newImage($width, $height, new ImagickPixel('red'));
        $img->setImageFormat('png');
        $img->writeImage($path);

        return $path;
    }

    public function validateTile(Tile $tile, UploadedFile $image): bool {
        $tile->setStatus(Tile::STATUS_FINISHED);

        $this->entityManager->flush();

        $root = $this->tileRepository->getStorageDirectory($tile);

        try {
//            echo __method__." move ".$root."/".$tile->getId() . '.' . $tile->getImageExtension()."\n";
            @mkdir($root);
            $image->move(
                $root,
                $tile->getId().'.'.$tile->getImageExtension());
        } catch (FileException $e) {
//            echo __method__.' '.$e."\n";
            return false;
        }

        if ($this->frameRepository->isFinished($tile->getFrame())) {
            $this->frameService->onFinish($tile->getFrame());
        }

        return true;
    }
}