<?php

namespace App\Tests\Service;

use App\Entity\Task;
use App\Repository\BlendRepository;
use App\Service\TaskService;
use App\Tests\ToolsService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskServiceTest extends KernelTestCase {
    private EntityManager $entityManager;
    private TaskService $taskService;
    private BlendRepository $blendRepository;
    private ToolsService $toolsService;

    protected function setUp(): void {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager(); // self::$container
        $this->blendRepository = self::$container->get('App\Repository\BlendRepository');
        $blendService = self::$container->get('App\Service\BlendService');
        $this->taskService = self::$container->get('App\Service\TaskService');
        $this->toolsService = new ToolsService($blendService);
    }

    public function testResetTask(): void {
        $blendId = $this->toolsService->addBlend("full", 10, 1, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);
        // add a task
        $task = new Task();
        $task->setBlend($blend);
        $task->setType(Task::TYPE_GENERATE_ZIP);
        $task->setStatus(Task::STATUS_RUNNING);
        $task->setStartTime((new \DateTime()));

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        $this->assertTrue($this->taskService->reset($task));
        $this->entityManager->flush(); // flush is not part of the service

        $this->entityManager->refresh($task);

        $this->assertEquals(Task::STATUS_WAITING, $task->getStatus());
        $this->assertNull($task->getStartTime());
        $this->assertEquals(Task::TYPE_GENERATE_ZIP, $task->getType()); // reset a task should not change its type
    }
}