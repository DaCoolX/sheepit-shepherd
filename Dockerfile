FROM jejem/php:8.1-apache
MAINTAINER Laurent Clouet <laurent.clouet@nopnop.net>

ARG FULL_VERSION=dev
ENV VERSION=$FULL_VERSION

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install \
	cron \
	logrotate \
	rsyslog \
	orphan-sysvinit-scripts \
	htop \
	nano \
	python3-openimageio \
	ffmpeg

RUN apt-get clean && \
	apt-get autoclean && \
	rm -rf /var/lib/apt/lists/*

RUN echo "memory_limit = 5G" >> /usr/local/etc/php/conf.d/custom.ini && \
	echo "max_execution_time = 120" >> /usr/local/etc/php/conf.d/custom.ini && \
	echo "post_max_size = 550M" >> /usr/local/etc/php/conf.d/custom.ini && \
	echo "upload_max_filesize = 550M" >> /usr/local/etc/php/conf.d/custom.ini

COPY docker/etc/logrotate.d/shepherd /etc/logrotate.d/
RUN chmod 644 /etc/logrotate.d/shepherd

RUN sed -i -e 's/\/var\/www\/html/\/var\/www\/html\/public/' /etc/apache2/sites-available/000-default.conf

COPY --chown=www-data:www-data . /var/www/html
RUN ln -fs /etc/shepherd/.env.local /var/www/html/.env.local

RUN chown -h www-data:www-data /var/www/html/.env.local

RUN runuser -s /bin/bash -c "cd  /var/www/html && composer install --no-dev --no-progress --no-suggest --optimize-autoloader" - www-data

COPY docker/etc/logrotate.d/shepherd /etc/logrotate.d/
RUN chmod 644 /etc/logrotate.d/shepherd

COPY docker/etc/cron.d/shepherd-tasks /etc/cron.d/
RUN chmod 644 /etc/cron.d/shepherd-tasks

COPY docker/etc/ImageMagick-6/policy.xml /etc/ImageMagick-6/
RUN chmod 644 /etc/ImageMagick-6/policy.xml

COPY docker/entrypoint.sh /
RUN chmod 755 /entrypoint.sh

VOLUME ["/etc/shepherd", "/var/www/html/var/log", "/var/lib/shepherd"]
ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
