<?php

namespace App\Repository;

use App\Entity\Blend;
use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TaskRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Task::class);
    }

    public function findOneAvailable(): ?Task {
        // algo:
        //
        // Only one heavy io task (zip and mp4 generation) can be run at the same time across all workers.
        //
        // a. search all distinct blends
        // b. foreach blend do priority:
        //          1: zip&mp4 if there is no generate_frame
        //          2: generate_frame & notification
        // c. if no task found do remaining tasks
        //          3. thumbnail
        //          4: delete

        $is_running_heavy_task = ($this->count(['status' => Task::STATUS_RUNNING, 'type' => Task::TYPE_GENERATE_MP4_FINAL]) + $this->count(['status' => Task::STATUS_RUNNING, 'type' => Task::TYPE_GENERATE_MP4_PREVIEW]) + $this->count(['status' => Task::STATUS_RUNNING, 'type' => Task::TYPE_GENERATE_ZIP])) > 0;
        $depend_tasks = [];

        foreach ($this->getDistinctBlends() as $blend_id) {
            $depend_tasks[$blend_id] = $this->count(['blend' => $blend_id, 'type' => Task::TYPE_GENERATE_FRAME]);

            // if there is no priority-1 task (generate_frame) them do priority-2 (project who must have ZERO generate_frame task)
            if ($is_running_heavy_task == false && $depend_tasks[$blend_id] == 0) {
                $qb = $this->_em->createQueryBuilder()
                    ->select('t')
                    ->from($this->_entityName, 't')
                    ->andWhere('t.status = :waiting')
                    ->andWhere('t.blend = :blend_id')
                    ->andWhere('t.type = :type_mp4_final OR t.type = :type_mp4_preview OR t.type = :type_zip')
                    ->setParameter('type_mp4_final', Task::TYPE_GENERATE_MP4_FINAL)
                    ->setParameter('type_mp4_preview', Task::TYPE_GENERATE_MP4_PREVIEW)
                    ->setParameter('type_zip', Task::TYPE_GENERATE_ZIP)
                    ->setParameter('waiting', Task::STATUS_WAITING)
                    ->setParameter('blend_id', $blend_id)
                    ->setMaxResults(1);

                $task = $qb->getQuery()->getOneOrNullResult();

                if (is_object($task)) {
                    return $task;
                }
            }

            // search for priory 1 tasks
            $qb = $this->_em->createQueryBuilder()
                ->select('t')
                ->from($this->_entityName, 't')
                ->andWhere('t.status = :waiting')
                ->andWhere('t.type = :type_mp4_final OR t.type = :type_mp4_preview OR t.type = :type_validate OR t.type = :type_zip OR t.type = :type_generate')
                ->setParameter('type_mp4_final', Task::TYPE_NOTIFY_MP4_FINAL)
                ->setParameter('type_mp4_preview', Task::TYPE_NOTIFY_MP4_PREVIEW)
                ->setParameter('type_validate', Task::TYPE_VALIDATE_TILE)
                ->setParameter('type_zip', Task::TYPE_NOTIFY_ZIP)
                ->setParameter('type_generate', Task::TYPE_GENERATE_FRAME)
                ->setParameter('waiting', Task::STATUS_WAITING)
                ->setMaxResults(1);

            $task = $qb->getQuery()->getOneOrNullResult();

            if (is_object($task)) {
                return $task;
            }
        }

        // everything else: delete and thumbnail
        $qb = $this->_em->createQueryBuilder()
            ->select('t')
            ->from($this->_entityName, 't')
            ->andWhere('t.status = :waiting')
            ->andWhere('t.type = :thumb_frame OR t.type = :thumb_tile OR t.type = :delete_blend')
            ->setParameter('thumb_tile', Task::TYPE_GENERATE_TILE_THUMBNAIL)
            ->setParameter('thumb_frame', Task::TYPE_GENERATE_FRAME_THUMBNAIL)
            ->setParameter('delete_blend', Task::TYPE_DELETE_BLEND)
            ->setParameter('waiting', Task::STATUS_WAITING)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return string[]
     */
    public function getDistinctRunningBlends(): array {
        $runningBlends = [];

        $data = $this->_em
            ->createQuery("SELECT DISTINCT(t.blend) as blend FROM App\Entity\Task t WHERE t.status = :running")
            ->setParameter('running', Task::STATUS_RUNNING)
            ->getArrayResult();

        foreach ($data as $values) {
            $runningBlends[$values['blend'] ]= $values['blend'];
        }

        return $runningBlends;
    }

    /**
     * @return string[]
     */
    public function getDistinctBlends(): array {
        $blends = [];

        $data = $this->_em
            ->createQuery("SELECT DISTINCT(t.blend) as blend FROM App\Entity\Task t")
            ->getArrayResult();

        foreach ($data as $values) {
            $blends[$values['blend']]= $values['blend'];
        }

        return $blends;
    }

    /**
     * @return Task[]
     */
    public function findDeadTasks(): array {
        // TODO: do it in QDL instead of PHP, it will be faster :)
        $min =  (new \DateTime())->setTimestamp(time() - Task::MAX_RUNNING_TIME);
        $ret = [];

        foreach ($this->findBy(['status' => Task::STATUS_RUNNING]) as $task) {
            /** @var Task $task */
            if ($task->getStartTime() != null && $task->getStartTime() < $min && $task->getPID() > 0) {
                // check if the process for the task is alive, since ONE TaskSingleExecuteCommand is launch per task it should be unique
                if (posix_getpgid($task->getPID()) == false) {
                    $ret [] = $task;
                }
            }
        }

        return $ret;
    }

    /**
     * @return array, keys 'total_task', 'queue_position', 'tasks'
     */
    public function getTaskPositionQueueFor(Blend $blend): array {
        $total_task = $this->count([]);
        $task_count = $this->count(['blend' => $blend->getId()]);

        // first the first/oldest task for that project
        $qb = $this->_em->createQueryBuilder()
            ->select('t')
            ->from($this->_entityName, 't')
            ->andWhere('t.blend = :blend_id')
            ->andWhere('t.type = :take_type')
            ->addOrderBy('t.id', 'ASC')
            ->setParameter('blend_id', $blend->getId())
            ->setParameter('take_type', Task::TYPE_GENERATE_ZIP)
            ->setMaxResults(1);

        /** @var ?Task $first_task */
        $first_task = $qb->getQuery()->getOneOrNullResult();

        if (is_null($first_task)) {
            $queue_position = 0;
        }
        else {
            $queue_position = $this->_em->createQueryBuilder()
                ->select('COUNT(t)')
                ->from($this->_entityName, 't')
                ->where('t.id <= :start')
                ->setParameter('start', $first_task->getId())
                ->getQuery()->getSingleScalarResult();
        }

        return [$total_task, $queue_position, $task_count];
    }
}
