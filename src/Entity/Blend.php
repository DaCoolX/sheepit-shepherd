<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BlendRepository")
 * @ORM\Table(name="blend")
 *
 */
class Blend {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private int $id;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank
     *
     */
    private float $framerate;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     *
     */
    private int $width;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     *
     */
    private int $height;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\NotBlank
     *
     */
    private ?float $size;

    /**
     * Generate mp4 videos
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $generateMp4;

    /**
     * @var Collection<int, Frame>
     * @ORM\OneToMany(targetEntity="Frame", mappedBy="blend")
     */
    private Collection $frames;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $ownerToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $ownerTokenValidity;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private string $thumbnailToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $thumbnailTokenValidity;

    public function __construct() {
        $this->frames = new ArrayCollection();
    }

    public function __toString(): string {
        return 'Blend(id: '.$this->id.' framerate: '.$this->framerate.')';
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }

    public function getFramerate(): ?float {
        return $this->framerate;
    }

    public function setFramerate(float $framerate): self {
        $this->framerate = $framerate;

        return $this;
    }

    public function getWidth(): ?int {
        return $this->width;
    }

    public function setWidth(int $width): self {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?int {
        return $this->height;
    }

    public function setHeight(int $height): self {
        $this->height = $height;

        return $this;
    }

    public function getSize(): ?float {
        return $this->size;
    }

    public function setSize(float $size): self {
        $this->size = $size;

        return $this;
    }

    /**
     * @return Collection|Frame[]
     */
    public function getFrames(): Collection {
        return $this->frames;
    }

    public function addFrame(Frame $frame): self {
        if (!$this->frames->contains($frame)) {
            $this->frames[] = $frame;
            $frame->setBlend($this);
        }

        return $this;
    }

    public function removeFrame(Frame $frame): self {
        if ($this->frames->contains($frame)) {
            $this->frames->removeElement($frame);

            // set the owning side to null (unless already changed)
            if ($frame->getBlend() === $this) {
                $frame->setBlend(null);
            }
        }

        return $this;
    }

    public function getOwnerToken(): ?string {
        return $this->ownerToken;
    }

    public function setOwnerToken(?string $ownerToken): self {
        $this->ownerToken = $ownerToken;

        return $this;
    }

    public function getOwnerTokenValidity(): ?\DateTimeInterface {
        return $this->ownerTokenValidity;
    }

    public function setOwnerTokenValidity(?\DateTimeInterface $ownerTokenValidity): self {
        $this->ownerTokenValidity = $ownerTokenValidity;

        return $this;
    }

    public function getThumbnailToken(): ?string {
        return $this->thumbnailToken;
    }

    public function setThumbnailToken(?string $thumbnailToken): self {
        $this->thumbnailToken = $thumbnailToken;

        return $this;
    }

    public function getThumbnailTokenValidity(): ?\DateTimeInterface {
        return $this->thumbnailTokenValidity;
    }

    public function setThumbnailTokenValidity(?\DateTimeInterface $thumbnailTokenValidity): self {
        $this->thumbnailTokenValidity = $thumbnailTokenValidity;

        return $this;
    }

    public function getGenerateMp4(): ?bool {
        return $this->generateMp4;
    }

    public function setGenerateMp4(?bool $generateMp4): self {
        $this->generateMp4 = $generateMp4;

        return $this;
    }
}
