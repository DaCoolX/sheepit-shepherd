<?php

namespace App\Command;

use App\Tool\NetworkUsage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command that save the current network usage
 */
class TasksMonitoringCommand extends Command {
    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingNativeTypeHint
     * @var string
     */
    protected static $defaultName = 'shepherd:monitoring';

    protected function configure(): void {
        $this->setDescription('Generate monitoring value');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        (new NetworkUsage())->generate();

        return Command::SUCCESS;
    }
}
