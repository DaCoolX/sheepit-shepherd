<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command that execute cleanup failed upload files.
 *
 * Yes we could have use the original tmpreaper but we had configuration issue on whitelisting files
 */
class TmpReaperCommand extends Command {
    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingNativeTypeHint
     * @var string
     */
    protected static $defaultName = 'shepherd:tmpreaper';

    protected function configure(): void {
        $this->setDescription('Cleanup failed upload files');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        // if the connection broke during the upload, the php process might not be yet created
        // and it might not be aware that it must clean up the file.
        // We need to remove those old files from time to time.
        // They are in the /tmp and start with php
        // Example: /tmp/php01pKX6:                            PNG image data, 1920 x 1080, 8-bit/color RGBA, non-interlaced

        $old = time() - 48 * 3600;

        foreach (glob(sys_get_temp_dir().'/php*') as $path) {
            $infos = new \SplFileInfo($path);

            if (is_file($path) && $infos->getATime() < $old) {
                @unlink($infos);
            }
        }

        foreach (glob(sys_get_temp_dir().'/*png') as $path) {
            $infos = new \SplFileInfo($path);

            if (is_file($path) && $infos->getATime() < $old) {
                @unlink($infos);
            }
        }

        return Command::SUCCESS;
    }
}
