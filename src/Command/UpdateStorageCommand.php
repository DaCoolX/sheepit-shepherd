<?php

namespace App\Command;

use App\Repository\BlendRepository;
use App\Service\BlendService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Update storage used by the blends
 *
 *     $ php bin/console shepherd:update-storage-usage
 *
 */
class UpdateStorageCommand extends Command {
    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingNativeTypeHint
     * @var string
     */
    protected static $defaultName = 'shepherd:update-storage-usage';

    private BlendRepository $blendRepository;
    private BlendService $blendService;

    public function __construct(BlendRepository $blendRepository, BlendService $blendService) {
        parent::__construct();

        $this->blendRepository = $blendRepository;
        $this->blendService = $blendService;
    }

    protected function configure(): void {
        $this->setDescription('Update storage used by blends');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ($this->blendRepository->findAll() as $blend) {
            $this->blendService->setSize($blend);
        }

        return Command::SUCCESS;
    }
}
