<?php

namespace App\Command;

use App\Entity\Task;
use App\Monitoring\MonitoringCpu;
use App\Repository\TaskRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Lock\LockInterface;
use Symfony\Component\Lock\Store\FlockStore;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Process\PhpExecutableFinder;

/**
 * A console command that execute waiting task.
 *
 *     $ php bin/console app:task
 *
 */
class TasksExecuteCommand extends Command {
    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingNativeTypeHint
     * @var string
     */
    protected static $defaultName = 'shepherd:tasks';
    protected const MAX_EXECUTION_TIME = 1800; // in seconds

    private ContainerBagInterface $containerBag;
    private LoggerInterface $logger;
    private EntityManagerInterface $entityManager;
    private TaskRepository $taskRepository;
    private ?LockInterface $lockWorker = null;
    private LockFactory $lockFactory;

    private DateTime $start;

    public function __construct(ContainerBagInterface $containerBag, EntityManagerInterface $entityManager, TaskRepository $taskRepository, LoggerInterface $logger) {
        parent::__construct();

        $this->containerBag = $containerBag;
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->taskRepository = $taskRepository;

        $store = new FlockStore(sys_get_temp_dir());
        $this->lockFactory = new LockFactory($store);

        $this->start = new DateTime();

    }

    protected function configure(): void {
        $this->setDescription('Execute all async tasks (zip, mp4 generation, ....)');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->canWork()) {
            do {
                $status = $this->workOneTime();
            }
            while ($status == true && $this->stillHaveTime());
        }

        $this->shutdown();

        return Command::SUCCESS;
    }

    /**
     * @return bool|Task true -> no task, false -> can not find task, object -> found a task
     */
    private function nextTask(): bool|Task {
        $monitoring = new MonitoringCpu();

        if ($monitoring->isOverloaded()) {
            return false;
        }

        $lock = $this->lockFactory->createLock('shepherd.task.search');

        $ret = false;

        $jobfindattempts = 0;
        $jobfindattemptsmax = 10;

        while ($ret == false && $jobfindattempts <= $jobfindattemptsmax) {
            if ($lock->acquire(true)) {

                $ret = $this->nextTasksActual();
                $lock->release();
                
                if (is_object($ret)) {
                    return $ret;
                }
            }

            sleep(rand(1, 3));
            $jobfindattempts++;
        }

        return $ret;
    }

    /**
     * @return bool|Task true -> no task, false -> can not find task, object -> found a task
     */
    private function nextTasksActual(): bool|Task {
        if ($this->taskRepository->count([]) == 0) {
            return true;
        }

        /** @var ?Task $task */
        $task = $this->taskRepository->findOneAvailable();

        if (is_object($task)) {
            $task->setStatus(Task::STATUS_RUNNING);
            $task->setStartTime(new DateTime());
            $this->entityManager->flush();
            return $task;
        }
        else {
            return false;
        }
    }

    private function workOneTime(): bool {
        $task = $this->nextTask();

        if ($task === true) { // no job
            return false;
        }
        elseif ($task === false) { // error
            return false;
        }
        elseif (is_object($task)) {
            $this->executeTask($task);
            return true; // silent the error for $this->work()
        }
        else {
            return false;
        }
    }

    private function executeTask(Task $task): bool {
        $this->logger->debug(__method__." ".$task->getId());

        $phpBinaryFinder = new PhpExecutableFinder();
        $phpBinaryPath = $phpBinaryFinder->find();

        $cmd = $phpBinaryPath.' '.__DIR__.'/../../bin/console '.TaskSingleExecuteCommand::$defaultName.' '.$task->getId();
        exec($cmd);

        return true;
    }

    private function canWork(): bool {
        if (intval($this->containerBag->get('concurrent_tasks')) != 0) {
            $concurrent_tasks = intval($this->containerBag->get('concurrent_tasks'));
        }
        else {
            $concurrent_tasks = intval(rtrim(shell_exec('nproc')));
        }
                
        for ($i = 1; $i <= $concurrent_tasks; $i++) {
            $lock = $this->lockFactory->createLock("shepherd.task.$i.pid");

            if ($lock->acquire(false)) {
                $this->lockWorker = $lock;
                return true;
            }
        }

        return false;
    }

    /**
     * Only works for 30min to avoid any big memory leak
     */
    private function stillHaveTime(): bool {
        return (new DateTime())->getTimestamp() - $this->start->getTimestamp() < self::MAX_EXECUTION_TIME;
    }

    private function shutdown(): void {
        if (is_null($this->lockWorker) == false) {
            $this->lockWorker->release();
        }
    }

}
