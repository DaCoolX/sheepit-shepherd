<?php

namespace App\Command;

use App\Entity\Task;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use App\Service\TaskService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaskDebugCommand extends Command {
    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingNativeTypeHint
     * @var string
     */
    protected static $defaultName = 'shepherd:task:execute:debug';

    private EntityManagerInterface $entityManager;
    private TileRepository $tileRepository;
    private FrameRepository $frameRepository;
    private BlendRepository $blendRepository;
    private TaskService $taskService;
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager, TaskService $taskService, TileRepository $tileRepository, FrameRepository $frameRepository, BlendRepository $blendRepository) {
        parent::__construct();

        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->tileRepository = $tileRepository;
        $this->frameRepository = $frameRepository;
        $this->blendRepository = $blendRepository;
        $this->taskService = $taskService;
    }

    protected function configure(): void {
        $this->setDescription('Execute task with argument');
        $this->addArgument('type', InputArgument::REQUIRED, 'Type');
        $this->addArgument('blend', InputArgument::REQUIRED, 'Blend id');
        $this->addArgument('frame', InputArgument::REQUIRED, 'Frame id');
        $this->addArgument('tile', InputArgument::REQUIRED, 'Tile id');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        $task = new Task();
        $task->setStatus(Task::STATUS_RUNNING);
        $task->setPID(getmypid());
        $task->setType($input->getArgument('type'));

        $tileObj = $this->tileRepository->find($input->getArgument('tile'));

        if (is_object($tileObj)) {
            $task->setTile($tileObj);
            $task->setFrame($tileObj->getFrame());
            $task->setBlend($tileObj->getFrame()->getBlend());
        }
        else {
            $frameObj = $this->frameRepository->find($input->getArgument('frame'));

            if (is_object($frameObj)) {
                $task->setFrame($frameObj);
                $task->setBlend($frameObj->getBlend());
            }
            else {
                $blendObj = $this->blendRepository->find($input->getArgument('blend'));

                if (is_object($blendObj)) {
                    $task->setBlend($blendObj);
                }
            }
        }

        $this->logger->debug(__method__.' task: '.$task);
        $ret = $this->taskService->execute($task);
        $this->entityManager->flush();

        return $ret ? Command::SUCCESS : Command::FAILURE;
    }
}
