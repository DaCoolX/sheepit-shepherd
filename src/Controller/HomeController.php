<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Default controller
 * @Route("/")
 */
class HomeController extends AbstractController {

    /**
     * @Route("/")
     */
    public function home(ContainerBagInterface $containerBag): RedirectResponse {
        return $this->redirect($containerBag->get('master_url'), 302);
    }
}