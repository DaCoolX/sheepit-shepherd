<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\Tile;
use App\Repository\TileRepository;
use App\Service\MasterService;
use App\Service\TileService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle request from renderer
 * @Route("/renderer")
 */
class RendererController {
    public static int $OK = 0;
    public static int $JOB_VALIDATION_ERROR_MISSING_PARAMETER = 300;
    public static int $JOB_VALIDATION_ERROR_BROKEN_MACHINE = 301; // in GPU the generated frame is black
    public static int $JOB_VALIDATION_ERROR_FRAME_IS_NOT_IMAGE = 302;
    public static int $JOB_VALIDATION_ERROR_UPLOAD_FAILED = 303;
    public static int $JOB_VALIDATION_ERROR_SESSION_DISABLED = 304; // missing heartbeat or broken machine
    public static int $JOB_VALIDATION_ERROR_WRONG_MD5 = 305; // only used on proto 3.2

    private TileRepository $tileRepository;
    private TileService $tileService;
    private LoggerInterface $logger;
    private MasterService $masterService;
    private EntityManagerInterface $entityManager;

    public function __construct(
        TileRepository $tileRepository,
        TileService $tileService,
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        MasterService $masterService) {

        $this->tileRepository = $tileRepository;
        $this->tileService = $tileService;
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->masterService = $masterService;
    }

    /**
     * Validate a image
     * Parameters:
     * id: Tile ID
     * rendertime: Job's render time
     * memoryused: Max memory used for the render
     * file: Frame to validate as form-data post
     * token: Renderer's token
     * Will return an xml with code:
     * 0 => No error
     * 300 => Missing parameter in request.
     * 301 => Client generated a broken frame (usually an too old gpu who generated black frame).
     * 302 => File uploaded was not an image.
     * 303 => Failed to upload the image to the server.
     * 304 => Client's session is disabled or dead.
     * something else => unknown error
     *
     * @Route("/tile", defaults={"_format"="xml"})
     */
    public function sendImage(Request $request): Response {
        $code = 901;

        $tileId = $request->get('id');
        $renderTime = $request->get('rendertime');
        $preptime = $request->get('preptime', 0);
        $memoryUsed = $request->get('memoryused');
        $token = $request->get('token');

        $file = $this->handleFileInput($request->files);

        if (is_int($file)) {
            $code = RendererController::$JOB_VALIDATION_ERROR_UPLOAD_FAILED;
        }
        elseif (is_null($token) || is_null($tileId) || is_null($renderTime) || is_null($memoryUsed)) {
            $code = RendererController::$JOB_VALIDATION_ERROR_MISSING_PARAMETER;
        }
        else {
            /** @var UploadedFile $file */
            /** @var ?Tile $tile */
            $tile = $this->tileRepository->find($tileId);

            if (is_null($tile)) {
                $code = 900; // TODO: ??
            }
            else {
                if ($tile->getStatus() == Tile::STATUS_PROCESSING && $tile->getToken() == $token) {
                    if ($tile->getToken() == $token) {
                        $code = $this->tileService->validate($tile, $file);

                        if ($code == 0) {
                            if ($this->masterService->notifyTileFinished($tile, $renderTime, $preptime, $memoryUsed) == false) {
                                // most like the master is down, let's retry later
                                $task = new Task();
                                $task->setType(Task::TYPE_VALIDATE_TILE);
                                $task->setTile($tile);
                                $task->setBlend($tile->getFrame()->getBlend());
                                $task->setDataA(strval($renderTime));
                                $task->setDataB(strval($memoryUsed));
                                $this->entityManager->persist($task);
                                $this->entityManager->flush();
                            }
                        }
                    }
                    else {
                        $this->logger->debug(__method__.' hide error to client tile: '.$tile.' renderer token: \''.$token.'\'');
                        $code = RendererController::$OK; // hide the error to the client
                    }
                }
            }
        }

        // if the validation of image failed or is refused, the uploaded file will be still there
        // remove it to avoid garbage on /tmp
        if ($file instanceof UploadedFile && file_exists($file->getRealPath())) {
            $this->logger->error(__method__.' file uploaded by renderer is still there, delete it. File: \''.$file->getRealPath().'\'');
            unlink($file->getRealPath());
        }

        $this->logger->debug(__method__.':'.__line__.' ret code: '.$code);
        return new Response($this->codeToValidateXml($code));
    }

    public function handleFileInput(FileBag $files): int|UploadedFile {
        if ($files->has('file')) {
            try {
                /** @var UploadedFile $uploadedFile */
                $uploadedFile = $files->get('file');

                if ($uploadedFile->isValid()) {
                    // TODO: check if the file is an image (302 => File uploaded was not an image.)
                    // $JOB_VALIDATION_ERROR_FRAME_IS_NOT_IMAGE
                    return $uploadedFile;
                }
                else {
                    $this->logger->error(__method__.' uploaded file is not valid error: '.$uploadedFile->getErrorMessage());
                }
            } catch (FileNotFoundException $e) {
                $this->logger->error(__method__.' '.$e);
                return -1;
            }
        }
        else {
            $this->logger->error(__method__.' FILES: '.serialize($_FILES));
        }

        return -2;
    }

    private function codeToValidateXml(int $code): string {
        $dom = new \DomDocument('1.0', 'utf-8');
        $root_node = $dom->createElement('jobvalidate');
        $root_node->setAttribute('status', strval($code));
        $dom->appendChild($root_node);

        return $dom->saveXML();
    }
}