<?php

namespace App\Controller;

use App\Entity\Blend;
use App\Entity\Task;
use App\Entity\Tile;
use App\Monitoring\MonitoringCpu;
use App\Repository\FrameRepository;
use App\Repository\TaskRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use App\Service\TileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle request for thumbnail
 * @Route("/thumb")
 */
class ThumbnailController extends AbstractController {

    private EntityManagerInterface $entityManager;
    private MonitoringCpu $monitoringCpu;
    private BlendService $blendService;
    private FrameRepository $frameRepository;
    private TileRepository $tileRepository;
    private TileService $tileService;
    private TaskRepository $taskRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        MonitoringCpu $monitoringCpu,
        BlendService $blendService,
        TileService $tileService,
        FrameRepository $frameRepository,
        TaskRepository $taskRepository,
        TileRepository $tileRepository) {

        $this->entityManager = $entityManager;
        $this->monitoringCpu = $monitoringCpu;
        $this->blendService = $blendService;
        $this->frameRepository = $frameRepository;
        $this->tileRepository = $tileRepository;
        $this->tileService = $tileService;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @Route("/{token}/{blend}/frame/{frameNumber}/thumbnail", methods="GET")
     */
    public function getFrameThumbnail(string $token, Blend $blend, int $frameNumber): BinaryFileResponse {
        if ($this->blendService->isThumbnailTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }

        $frame = $this->frameRepository->findOneBy(array('blend' => $blend->getId(), 'number' => $frameNumber));

        if (is_object($frame)) {
            $path = $this->frameRepository->getThumbnailPath($frame);

            if (file_exists($path) == false) {
                throw $this->createNotFoundException('file not found');
            }

            return $this->giveFile($path);
        }
        else {
            throw $this->createNotFoundException('file not found');
        }
    }

    /**
     * @Route("/{token}/{blend}/tile/{tile}/thumbnail", methods="GET")
     */
    public function getTileThumbnail(string $token, Blend $blend, Tile $tile): BinaryFileResponse {
        return $this->getTileThumbnailOnDemand($token, $blend, $tile, false);
    }

    /**
     * @Route("/{token}/{blend}/tile/{tile}/thumbnail/{ondemand}", methods="GET")
     */
    public function getTileThumbnailOnDemand(string $token, Blend $blend, Tile $tile, bool $ondemand): BinaryFileResponse {
        if ($this->blendService->isThumbnailTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }

        $path = $this->tileRepository->getThumbnailPath($tile);

        if (file_exists($path) == false) {
            if ($ondemand && $this->monitoringCpu->isOverloaded() == false) {
                // don't exist -> create it and remove the waiting creation task
                if ($this->tileService->generateThumbnail($tile)) {
                    $task = $this->taskRepository->findOneBy(array('tile' => $tile, 'type' => Task::TYPE_GENERATE_TILE_THUMBNAIL));

                    if (is_object($task)) {
                        $this->entityManager->remove($task);
                        $this->entityManager->flush();
                    }
                }
                else {
                    throw $this->createNotFoundException('file not found');
                }
            }
            else {
                throw $this->createNotFoundException('file not found');
            }
        }

        return $this->giveFile($path);
    }

    private function giveFile(string $path): BinaryFileResponse {
        try {
            $file = new File($path, true);
        } catch (FileNotFoundException $e) {
            throw $this->createNotFoundException('file not found');
        }

        header('Content-Type: '.$file->getMimeType());
        header('Content-Length: '.filesize($file));

        return $this->file($file, $file->getFilename(), ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
