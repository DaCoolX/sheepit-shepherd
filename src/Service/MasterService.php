<?php

namespace App\Service;

use App\Entity\Blend;
use App\Entity\Task;
use App\Entity\Tile;
use App\Repository\TileRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MasterService {
    private const TARGET_FAILED = 'failed.php';
    private const TARGET_SUCCESS = 'finish.php';

    private string $serverUrl;
    private LoggerInterface $logger;
    private TileRepository $tileRepository;

    public function __construct(ContainerBagInterface $containerBag, LoggerInterface $logger, TileRepository $tileRepository) {
        $this->serverUrl = $containerBag->get('master_url').'/api/shepherd/';
        $this->logger = $logger;
        $this->tileRepository = $tileRepository;
    }

    public function notifyGeneratedMP4Final(Blend $blend): bool {
        return $this->notify(self::TARGET_SUCCESS, 'mp4_final', $blend);
    }

    public function notifyGeneratedMP4FinalFailed(Blend $blend): bool {
        return $this->notify(self::TARGET_FAILED, 'mp4_final', $blend);
    }

    public function notifyGeneratedMP4Preview(Blend $blend): bool {
        return $this->notify(self::TARGET_SUCCESS, 'mp4_preview', $blend);
    }

    public function notifyGeneratedMP4PreviewFailed(Blend $blend): bool {
        return $this->notify(self::TARGET_FAILED, 'mp4_preview', $blend);
    }

    public function notifyGeneratedZIP(Blend $blend): bool {
        return $this->notify(self::TARGET_SUCCESS, 'zip', $blend);
    }

    public function notifyGeneratedZIPFailed(Blend $blend): bool {
        return $this->notify(self::TARGET_FAILED, 'zip', $blend);
    }

    public function notifyTileFinished(Tile $tile, int $renderTime, int $prepTime, int $memoryUser): bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.self::TARGET_SUCCESS, array(
                'body' => array(
                    'type' => 'tile',
                    'rendertime' => $renderTime,
                    'preptime' => $prepTime,
                    'memory_used' => $memoryUser,
                    'filesize' => filesize($this->tileRepository->getPath($tile)),
                    'tile' => $tile->getId(),
                ),
            ));

            if ($response->getStatusCode() == 200) {
                return true;
            }
            else {
                $this->logger->error('Failed to notify master of tileFinished, tile:'.$tile->getId().' http code '.$response->getStatusCode());
                return false;
            }
        } catch (TransportExceptionInterface $e) {
            $this->logger->error(__method__.' failed to validate tile on master '.$e);
            return false;
        }
    }

    public function notifyTileFailed(Tile $tile): bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.self::TARGET_FAILED, array(
                'body' => array(
                    'type' => 'tile',
                    'id' => $tile->getId(),
                ),
            ));

            if ($response->getStatusCode() == 200) {
                return true;
            }
            else {
                $this->logger->error('Failed to notify master for type: tile tile:'.$tile->getId().' http code '.$response->getStatusCode());
                return false;
            }
        }
        catch (TransportExceptionInterface $e) {
            $this->logger->error('Failed to notify master for type: tile tile:'.$tile->getId().' exception: '.$e->getMessage());
            return false;
        }
    }

    public function notifyTaskReset(Task $task): bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'reset.php', array(
                'body' => array(
                    'type' => $task->getType(),
                    'id' => $task->getId(),
                    'blend' => $task->getBlend(),
                ),
            ));
            return $response->getStatusCode() == 200;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function sendMonitoring(string $json): bool {
        $this->logger->error(__method__.' json: '.$json);

        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'monitoring.php', ['body' => $json]);
            return $response->getStatusCode() == 200;
        }
        catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    private function notify(string $target, string $type, Blend $blend): bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.$target, array(
                'body' => array(
                    'type' => $type,
                    'blend' => $blend->getId(),
                ),
            ));

            if ($response->getStatusCode() == 200) {
                return true;
            }
            else {
                $this->logger->error('Failed to notify master for type: '.$type.' blend:'.$blend->getId().' http code '.$response->getStatusCode());
                return false;
            }
        }
        catch (TransportExceptionInterface $e) {
            $this->logger->error('Failed to notify master for type: '.$type.' blend:'.$blend->getId().' exception: '.$e->getMessage());
            return false;
        }
    }
}