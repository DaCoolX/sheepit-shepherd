<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App;

use Exception;
use Imagick;
use ImagickPixel;

class Image {
    protected ?string $path;

    protected ?Imagick $resource;

    protected ?string $path_tga; // dirty hack because Imagick does not recognize a TARGA file if it does not have an tga extension

    /** @var  array<string, string> $format2extension */
    protected static array $format2extension = array(
        'bmp2' => 'bmp',
        'bmp3' => 'bmp',
        'jpeg' => 'jpg',
        'png24' => 'png',
        'png32' => 'png',
        'png8' => 'png',
    );

    public function __construct(?string $path_) {
        $this->path = $path_;
        $this->path_tga = null;
        $this->resource = null;
    }

    protected function open(): bool {
        if (is_file($this->path) == false) {
            return false;
        }

        try {
            $mypicture = new Imagick();
            $mypicture->readImage($this->path);
            $this->resource = $mypicture;

            return true;
        } catch (Exception $e) {
            // dirty hack because Imagick does not recognize a TARGA file if it does not have an tga extension
            $unique_name = sys_get_temp_dir().'/'.uniqid().'.tga';
            $tmp_file = $unique_name;
            copy($this->path, $tmp_file);

            try {
                $mypicture = new Imagick();
                $mypicture->readImage($tmp_file);
                $this->resource = $mypicture;
                $this->path_tga = $tmp_file;
                @unlink($unique_name);
                return true;
            } catch (Exception $e) {
                //	//Logger::debug("Image::open2 exception original path '".$this->path."' $e");
                @unlink($tmp_file);
                @unlink($unique_name);
            }

            return false;
        }
    }

    protected function close(): bool {
        $this->resource = null;

        if (is_null($this->path_tga) == false) {
            @unlink($this->path_tga);
        }

        return true;
    }

    public function isImage(): bool {
        $ret = $this->open();

        $this->close();

        return $ret;
    }

    public function generateThumbnail(int $width_, int $height_, string $output_file): bool {
        if ($this->isExr()) {
            return $this->generateThumbnailExr($width_, $height_, $output_file);
        }
        else {
            return $this->generateThumbnailNoLayer($width_, $height_, $output_file);
        }
    }

    protected function generateThumbnailNoLayer(int $width_, int $height_, string $output_file): bool {
        $ret = false;
        $isImage = $this->open();

        if ($isImage == true) {
            try {
                $this->resource->scaleImage($width_, $height_);
                $this->resource->setImageFormat('png');
                $this->resource->writeImage($output_file);
                $ret = true;
            } catch (Exception $e) {
                $ret = false;
            }
        }

        $this->close();

        return $ret;
    }

    protected function generateThumbnailExr(int $width_, int $height_, string $output_file): bool {
        // step 1: Extract "Combined" layer of the EXR and convert it to PNG
        $targetPNG = sys_get_temp_dir().'/'.uniqid().'.png';
        $pythonCommand = 'python3 '.__DIR__.'/../python/exr_to_png.py "'.$this->path.'" '.$targetPNG;
        exec($pythonCommand);

        // step 2:  Make a thumbnail of the PNG
        $im = new Image($targetPNG);
        $ret = $im->generateThumbnailNoLayer($width_, $height_, $output_file);
        unlink($targetPNG);
        return $ret;
    }

    public function fileExtension(): bool|string {
        $ret = false;
        $isImage = $this->open();

        if ($isImage == true) {
            try {
                $ret = strtolower($this->resource->getImageFormat());
            } catch (Exception $e) {
                $ret = false;
            }
        }

        $this->close();

        if (array_key_exists($ret, Image::$format2extension)) {
            $ret = Image::$format2extension[$ret];
        }

        return $ret;
    }

    public function mimeType(): bool|string {
        $ret = false;
        $isImage = $this->open();

        if ($isImage == true) {
            try {
                $ret = $this->resource->getImageFormat();
            } catch (Exception $e) {
                $ret = false;
            }
        }

        $this->close();

        return $ret;
    }

    public function getGeometry(): bool|array {
        $ret = false;
        $isImage = $this->open();

        if ($isImage == true) {
            try {
                $ret = $this->resource->getImageGeometry();
            } catch (Exception $e) {
                $ret = false;
            }
        }

        $this->close();

        return $ret;
    }

    public function assemble(array $lines): bool {
        foreach ($lines as $col) {
            foreach ($col as $path) {
                if (file_exists($path) == true) {
                    $im = new Image($path);

                    if ($im->isExr()) {
                        return $this->assembleExr($lines);
                    }
                    else {
                        return $this->assembleRegular($lines);
                    }
                }
            }
        }

        return false;
    }

    public function assembleFromSplit(array $files): bool {
        // background: 100%
        // second image: 50%
        // third image: 33%
        // 1/n ...


        $path = array_pop($files);
        $dest = new Imagick();

        try {
            $dest->readImage($path);

            if (Imagick::ALPHACHANNEL_UNDEFINED == $dest->getImageAlphaChannel()) { // or Imagick::ALPHACHANNEL_ACTIVATE
                $dest->setImageAlphaChannel(Imagick::ALPHACHANNEL_SET);
            }

            $i = 2;

            foreach ($files as $path) {
                $img = new Imagick();
                $img->readImage($path);

                if (Imagick::ALPHACHANNEL_UNDEFINED == $img->getImageAlphaChannel()) { // or Imagick::ALPHACHANNEL_ACTIVATE
                    $img->setImageAlphaChannel(Imagick::ALPHACHANNEL_SET);
                }

                $img->evaluateImage(Imagick::EVALUATE_DIVIDE, $i, Imagick::CHANNEL_ALPHA);
                $dest->compositeImage($img, imagick::COMPOSITE_DEFAULT, 0, 0);
                $i++;
            }

            $dest->setImageFormat('png');
            return $dest->writeImage($this->path);
        } catch (\ImagickException $e) {
            return false;
        }
    }

    protected function assembleRegular(array $lines): bool {
        $tile_geometry = null;

        $dest = new Imagick();

        foreach ($lines as $col) {
            $img = new Imagick();

            foreach ($col as $path) {
                if (file_exists($path) == true) {
                    $img->readImage($path);
                }
                else {
                    if ($tile_geometry == null) { // geometry have never been generated
                        foreach ($lines as $col) {
                            foreach ($col as $path) {
                                if (file_exists($path) == true) {
                                    $apicture = new Image($path);
                                    $geo = $apicture->getGeometry();

                                    if (is_array($geo)) {
                                        $tile_geometry = $geo;
                                        break;
                                    }
                                }
                            }
                        }

                        if ($tile_geometry == null) {
                            $tile_geometry = false; // no need to generate again (it will failed again)
                        }
                    }

                    if (is_array($tile_geometry)) {
                        $img->newImage($tile_geometry['width'], $tile_geometry['height'], new ImagickPixel('transparent'));
                    }
                }
            }

            $img->resetIterator();
            $col_img = $img->appendImages(false);

            $combined = $dest->addImage($col_img);
        }

        $dest->resetIterator();
        $combined = $dest->appendImages(true);

        $combined->setImageFormat('png');
        return $combined->writeImage($this->path);
    }

    protected function assembleExr(array $lines): bool {
        global $config;
        $root_dir = null;

        foreach ($lines as $col) {
            foreach ($col as $path) {
                if (file_exists($path) == true) {
                    $root_dir = dirname($path);
                    break;
                }
            }
        }

        $command = "PYTHON_EGG_CACHE=".$config['tmp_dir']." /usr/bin/python -B ".dirname(__FILE__)."/merge_exr.py ".count($lines)." \"$root_dir\" \"$this->path\"";
        shell_exec($command);
        return true;
    }

    public function isFullyBlack(): bool {
        if ($this->isExr()) {
            //Logger::debug('Image::isFullyBlack is disabled for EXR image');
            return false;
        }

        if ($this->isImage() == false) {
            //Logger::error('Image::isFullyBlack not an image');
            return false;
        }

        $cmd = 'convert '.$this->path.' -format "%[mean]" info:';
        exec($cmd.' 2>&1', $output);

        if (is_array($output) && count($output) == 1) {
            if ($output[0] == '0') {
                return true;
            }
        }

        return false;
    }

    public function isExr(): bool {
        return $this->mimeType() == 'EXR';
    }

    public function compareTo(string $other, bool $scale = true): bool {
        try {
            if ($this->open() == false) {
                //Logger::debug('Image::compareTo, failed to open file '.$this->path);
                return false;
            }

            $image2 = new Imagick();
            $image2->readImage($other);

            if ($scale) {
                // resize to the same dimension for better comparaison
                $this->resource->scaleImage(960, 540);
                $image2->scaleImage(960, 540);
            }

            // set the fuzz factor (must be done BEFORE reading in the images)
            $this->resource->SetOption('fuzz', '1%');

            // compare the images using METRIC=1 (Absolute Error)
            $result = $this->resource->compareImages($image2, 1);

            $this->close();

            return ($result[1] == 0);
        } catch (Exception $e) {
            //Logger::debug("Image::compareTo, excetion ".$e);
        }

        return false;
    }

    public function isSimilarTo(string $other): bool {
        try {
            if ($this->open() == false) {
                //	//Logger::debug(__method__.', failed to open file '.$this->path);
                return false;
            }

            $this_geometry = null;

            try {
                $this_geometry = $this->resource->getImageGeometry();
            } catch (Exception $e) {
                $this_geometry = null;
            }

            $other_img = new Image($other);
            $other_geometry = $other_img->getGeometry();

            if (is_array($other_geometry) && is_array($this_geometry)) {
                if ($this_geometry['height'] != $other_geometry['height'] && $this_geometry['width'] != $other_geometry['width']) {
                    return false;
                }
            }

            // resize to the same dimension for better comparaison
// 			$this->resource->scaleImage(960, 540);
            $image2 = new Imagick();
            $image2->readImage($other);
// 			$image2->scaleImage(960, 540);

            // set the fuzz factor (must be done BEFORE reading in the images)
            $this->resource->SetOption('fuzz', '1%');

            // compare the images using METRIC=1 (Absolute Error)
            $result = $this->resource->compareImages($image2, 1);

            $this->close();

            return $result[1] < 30;
        } catch (Exception $e) {
            //Logger::debug(__method__." exception ".$e);
        }

        return false;
    }
}
