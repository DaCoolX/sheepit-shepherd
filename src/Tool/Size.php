<?php

namespace App\Tool;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Size {
    public static function dirSize(string $directory): int {
        if (file_exists($directory) == false) {
            return 0;
        }

        $size = 0;

        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file) {
            $size += $file->getSize();
        }

        return $size;
    }

    public static function humanSize(int $size, string $unit = 'B'): string {
        $n = $size;

        if ($n < 0) {
            $n = -1 * $n;
        }

        if (1 <= ($n / 1000000000000) && ($n / 1000000000000) < 1000) {
            $str = sprintf("%.1f T", $n / 1000000000000);
        }
        elseif (1 < ($n / 1000000000) && ($n / 1000000000) < 1000) {
            $str = sprintf("%.1f G", $n / 1000000000);
        }
        elseif (1 < ($n / 1000000) && ($n / 1000000) < 1000) {
            $str = sprintf("%.1f M", $n / 1000000);
        }
        elseif (1 < ($n / 1000) && ($n / 1000) < 1000) {
            $str = sprintf("%.1f k", $n / 1000);
        }
        else {
            $str = sprintf("%.1f ", $n);
        }

        if ($size < 0) {
            $str = '-'.$str;
        }

        return $str.$unit;
    }
}