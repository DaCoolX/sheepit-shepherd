<?php

namespace App\Monitoring;

/**
 * Definition of a monitoring component
 */
abstract class MonitoringComponentAbstract {

    public abstract function getType(): string;

    public abstract function getValue(): float;

    public abstract function getHumanValue(): string;

    public function hasMax(): bool {
        return false;
    }

    public function getMax(): float {
        return 0.0; // should never be call if hasMax is false
    }
}
