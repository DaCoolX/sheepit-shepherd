<?php

namespace App\Monitoring;

class MonitoringRamPsi extends MonitoringComponentAbstract {

    public function getType(): string {
        return 'ram pressure';
    }

    public function getValue(): float {

        $file_path = '/proc/pressure/memory';

        // Read psi file 
        $file_contents = file_get_contents($file_path);

        // Regular expression to extract avg300 value
        $pattern = '/avg300=([\d.]+)/';

        // Match the pattern
        if (preg_match($pattern, $file_contents, $matches)) {
            // Extract the avg300 value
            $avg300 = (float) $matches[1];
        }
        else {
            // error handling: If pattern not found, set default value
            $avg300 = 0.00;
        }

        return $avg300;
    }
    
    public function isOverloaded(): bool {
        return $this->getValue() >= $this->getMax();
    }

    public function getHumanValue(): string {
        return $this->getValue().' / '.$this->getMax();
    }

    public function hasMax(): bool {
        return true;
    }

    public function getMax(): float {
        return 100.00;
    }
}