<?php

namespace App\Monitoring;

use App\Tool\Size;

class MonitoringRam extends MonitoringComponentAbstract {

    private float $current;
    private float $max;

    public function __construct() {
        $meminfo = array();

        foreach (explode("\n", trim(file_get_contents("/proc/meminfo"))) as $line) {
            [$key, $val] = explode(":", $line);
            $meminfo[$key] = intval(trim(str_replace(' kB', '', $val))) * 1000;
        }

        $this->current = $meminfo['MemTotal'] - $meminfo['MemAvailable'];
        $this->max = $meminfo['MemTotal'];

        // TODO: should the swap be sent too ? 'swap_used'  => $meminfo['SwapTotal'] - $meminfo['SwapFree']) - $meminfo['SwapCached'];
    }

    public function getType(): string {
        return 'ram';
    }

    public function getValue(): float {
        return $this->current;
    }

    public function hasMax(): bool {
        return true;
    }

    public function getMax(): float {
        return $this->max;
    }

    public function getHumanValue(): string {
        return Size::humanSize($this->getValue()).' / '.Size::humanSize($this->getMax());
    }

}
