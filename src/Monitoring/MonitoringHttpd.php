<?php

namespace App\Monitoring;

class MonitoringHttpd extends MonitoringComponentAbstract {
    public function getType(): string {
        return 'httpd';
    }

    public function getValue(): float {
        foreach (explode("\n", file_get_contents('http://localhost/server-status?auto')) as $line) {
            $data = explode(":", $line);

            if (count($data) == 2 && $data[0] == 'BusyWorkers') {
                return (int)($data[1]);
            }
        }

        return 0;
    }

    public function getHumanValue(): string {
        return $this->getValue().' workers';
    }
}