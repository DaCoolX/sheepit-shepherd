<?php

namespace App\Monitoring;

use App\Repository\BlendRepository;
use App\Tool\Size;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class MonitoringDisk extends MonitoringComponentAbstract {

    private ContainerBagInterface $containerBag;
    private BlendRepository $blendRepository;

    public function __construct(ContainerBagInterface $containerBag, BlendRepository $blendRepository) {
        $this->containerBag = $containerBag;
        $this->blendRepository = $blendRepository;
    }

    public function getType(): string {
        return 'disk';
    }

    public function getValue(): float {
        $total = 0;

        foreach ($this->blendRepository->findAll() as $blend) {
            $total += $blend->getSize();
        }

        return $total;
    }
    
    public function getTMPFREE(): float {
        return disk_free_space(sys_get_temp_dir());
    }

    public function getHumanValue(): string {
        return 'used: '.Size::humanSize($this->getValue()).' '."<br>".'free: '.Size::humanSize($this->getMax()).' '."<br>".'TMPfree: '.Size::humanSize($this->getTMPFREE());
    }

    public function hasMax(): bool {
        return true;
    }

    public function getMax(): float {
        return disk_free_space($this->containerBag->get('storage_dir'));
    }
}
